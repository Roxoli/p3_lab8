﻿using System;
using System.Collections.Generic;

namespace p3_lab8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var inksList = new List<Ink> { new Ink("blue", 10), new Ink("red", 15), new Ink("Grey", 25) };
            var printer = new Printer(88, inksList);
            printer.OutOfPaperEvent += OutOfPaperEventHandler;
            printer.OutOfInkEvent += OutOfInkEventHandler;
            printer.Print(100);
        }
        static void OutOfPaperEventHandler(object obj, EventArgs args)
        {
            Console.WriteLine("Not enaugh paper");
        }
        static void OutOfInkEventHandler(object obj, OutOfInkEventArgs args)
        {
            Console.WriteLine("Out of ink");
        }
    }
}
