﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p3_lab8
{
    class Printer
    {
        public event EventHandler OutOfPaperEvent;
        public event EventHandler<OutOfInkEventArgs> OutOfInkEvent;
        public int pageCounter;
        public List<Ink> InkColors = new List<Ink> { new Ink("blue", 10), new Ink("red", 15), new Ink("Grey", 20) };

        public Printer(int pages, List<Ink> ic)
        {
            OutOfPaperEvent += OutOfPaperEventHandler;
            OutOfInkEvent += OutOfInkEventHandler;
            pageCounter = pages;
            InkColors = ic;
        }

        Random rand = new Random();
        public void Print(int pagesToPrint)
        {
            for (int i = 0; i < pagesToPrint; i++)
            {
               
                if(pageCounter < 1 )
                {
                    OutOfPaperEvent.Invoke(this, EventArgs.Empty);
                    return;
                }

                pageCounter -= 1;

                foreach (var item in InkColors)
                         {
                            if (item.AmountOfInk < 1)
                            {
                                OutOfInkEvent.Invoke(this, new OutOfInkEventArgs(item.InkColor, i));
                                return;
                            }
                                item.AmountOfInk -= 1;

                         }
            }
            

            
        }
        private void OutOfPaperEventHandler(object obj, EventArgs args)
        {
            Console.WriteLine($"{DateTime.Now} : [Printerlog]: Not enaugh paper");
        }
        private void OutOfInkEventHandler(object obj, OutOfInkEventArgs args)
        {
            Console.WriteLine($"{DateTime.Now} : [Printerlog]: Out of {0} ink, {1} page(s) printed)", args.InkColor, args.NumOfCopies);
        }
    }
}
