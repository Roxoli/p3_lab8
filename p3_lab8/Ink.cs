﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p3_lab8
{
    class Ink
    {
        public string InkColor { get; set; }
        public float AmountOfInk { get; set; }

        public Ink(string ic, float aoi)
        {
            this.InkColor = ic;
            this.AmountOfInk = aoi;
        }
    }
}
