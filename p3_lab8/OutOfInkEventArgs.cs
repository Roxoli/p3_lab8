﻿using System;
using System.Collections.Generic;
using System.Text;

namespace p3_lab8
{
    class OutOfInkEventArgs
    {
        public string InkColor { get; set; }
        public int NumOfCopies { get; set; }
        public OutOfInkEventArgs(string ic, int noc)
        {
            this.InkColor = ic;
            this.NumOfCopies = noc;
        }
    }
}
